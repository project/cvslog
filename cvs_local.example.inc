<?php

/**
 * @file
 * Site-specific code that needs to be customized. The really
 * egregious hacks for running on drupal.org are all in here.
 * So, if you're setting up the cvs.module on another site, chances
 * are good you'll want to modify or replace the functions in here
 * with something appropriate for your needs.
 *
 */

/**
 * Returns an object of version number values based on the given tag.
 */
function cvs_local_get_version_from_tag($tag, $project) {
  // First, split on '--', if it's there, that tells us API vs. regular
  list($first_ver, $second_ver) = explode('--', $tag->tag);
  $first = explode('-', $first_ver, 5);
  $second = empty($second_ver) ? array() : explode('-', $second_ver, 3);

  if ($project->nid == 3060) {
    if ($first[1] >= 5) {
      $version->version_major = $first[1];
      if (isset($first[2])) {
        $version->version_patch = $first[2];
        $version->version_extra = _cvs_get_extra($first[3].$first[4]);
      }
      else {
        $version->version_patch = 'x';
        $version->version_extra = 'dev';
      }
    }
    else {
      if (isset($first[3])) {
        // Looks like "DRUPAL-X-Y-Z". Ignore the 'DRUPAL' part in $first[0].
        $version->version_major = $first[1];
        $version->version_minor = $first[2];
        $version->version_patch = $first[3];
        $version->version_extra = _cvs_get_extra($first[4]);
      }
      else {
        // Looks like "DRUPAL-X-Y", must be like "DRUPAL-4-7" (branch)
        $version->version_major = $first[1];
        $version->version_minor = $first[2];
        $version->version_patch = 'x';
        $version->version_extra = 'dev';
      }
    }
  }
  else {
    // Contrib tags don't need any special cases.  If there's nothing
    // after the '--' (the default stable branch), we want "-1.x-dev".
    $version->version_major = isset($second[0]) ? $second[0] : 1;
    $version->version_patch = isset($second[1]) ? $second[1] : 'x';
    $version->version_extra = $tag->branch ? 'dev' : (isset($second[2]) ? _cvs_get_extra($second[2]) : '');
  }
  if ($tree = project_release_get_api_taxonomy()) {
    // If we're using the compatibility taxonomy, find the right tid.
    // For both core and contrib, it's set by what we saw before the '--',
    // namely, the stuff in the $first array...
    if (!isset($first[2]) || ($project->nid == 3060 && $first[1] >= 5)) {
      $target = "$first[1].x";
    }
    else {
      $target = "$first[1].$first[2].x";
    }
    foreach ($tree as $i => $term) {
      if ($term->name == $target) {
        $version->version_api_tid = $term->tid;
        break;
      }
    }
  }
  // TODO: add code to validate we parsed something reasonable, and ignore bad tags.
  return $version;
}

/**
 * Returns the CVS tag that should match the given version information
 */
function cvs_local_get_tag_from_version($version, $project) {
  $tag = 'DRUPAL-';  // all tags start with this.
  $parts[] = $version->version_major;
  if (isset($version->version_minor)) {
    $parts[] = $version->version_minor;
  }
  if (isset($version->version_patch)) {
    $parts[] = $version->version_patch;
  }
  else {
    $is_branch = TRUE;
  }
  if (isset($version->version_extra)) {
    if ($version->version_extra != 'dev') {
      $parts[] = strtoupper($version->version_extra);
      // sadly, this will be wrong, since we strip out '_' and '-',
      // but we don't really care that much, since this is primarily
      // going to be used for branches, not tags.
    }
    else {
      $is_branch = TRUE;
    }
  }
  if ($project->nid != 3060) {
    // For contrib, we have to prepend the Drupal core compatibility.
    if (!empty($version->version_api_tid)) {
      $term = taxonomy_get_term($version->version_api_tid);
      $api = preg_replace(array('/\.x/', '/\./'), array('', '-'), $term->name);
      $tag .= $api;
    }
    if ($is_branch && $api < 6 && $version->version_major == 1) {
      // Evil special-case, we're done.
      return $tag;
    }
    $tag .= '--';
  }
  $tag .= implode('-', $parts);
  return $tag;
}

function _cvs_get_extra($str) {
  return preg_replace('/[_-]/', '', strtolower($str));
}

/**
 * drupal.org specific customizations of the release node form
 */
function cvs_local_alter_project_release_form(&$form, &$form_state) {
  $node = $form['#node'];
  if (isset($node->project_release['tag'])) {
    $tag = $node->project_release['tag'];
  }
  elseif (isset($form_state['values']['cvs_tag']['tag'])) {
    $tag = $form_state['values']['cvs_tag']['tag'];
  }
  if (isset($tag) && $tag === 'HEAD') {
    $form['version']['num']['#description'] = t('For releases being rebuilt from the trunk of the CVS repository (HEAD), you must use "x" for the %patch version and "dev" in the %extra to indicate it is a development snapshot. Furthermore, you should probably use "1" for the %major version to indicate that this is the initial version compatible with the selected version of Drupal core, unless you are using HEAD as a place to develop new features still compatible with a previous version of core, in which case you should use "2" or higher, as appropriate.', array('%major' => t('Major'), '%patch' => t('Patch-level'), '%extra' => t('Extra identifier')));
    unset($form['project_release']['version_extra']['#description']);
  }
}

function cvs_local_project_release_form_pre_render(&$form) {
  $form['version']['num']['version_major']['#required'] = TRUE;
  $form['version']['num']['version_patch']['#required'] = TRUE;
}

/**
 * Decides if the given version object has enough information for
 * us to move on to the next page.
 */
function cvs_local_version_is_valid($version, $tag, $project) {
  if (empty($version)) {
    return FALSE;
  }
  $is_valid = TRUE;
  if ($project->nid == 3060) {
    // Core is obvious: we just need to ensure we've got major and patch.
    // Everything else is optional.
    if (!isset($version->version_major)) {
      $is_valid = FALSE;
      form_set_error('version_major', t('Major version number is required.'));
    }
    elseif ($version->version_major >= 5 && isset($version->version_minor)) {
      form_set_error('version_minor', t('You should not specify a minor version number if the major version is greater than or equal to 5.'));
      $is_valid = FALSE;
    }
    if (!isset($version->version_patch)) {
      $is_valid = FALSE;
      form_set_error('version_patch', t('Patch-level version number is required.'));
    }
  }
  else {
    if (!isset($version->version_api_tid)) {
      $is_valid = FALSE;
      $vid = _project_release_get_api_vid();
      if (isset($_POST['taxonomy'][$vid])) {
        // We only want to flag an error if they've tried to advance
        form_set_error($vid, t('You must select what version of Drupal core this release is compatible with.'));
      }
    }
    if (!isset($version->version_major)) {
      $is_valid = FALSE;
      form_set_error('version_major', t('Major version number is required, and must be a number.'));
    }
    if (!isset($version->version_patch)) {
      $is_valid = FALSE;
      form_set_error('version_patch', t('Patch-level version number is required, and must be either a number or the letter "x".'));
    }
  }
  if ($tag->branch) {
    if ($version->version_patch != 'x') {
      $is_valid = FALSE;
      form_set_error('version_patch', t('The %patch must be "x" for snapshot releases from a branch (or HEAD).', array('%patch' => 'Patch-level')));
    }
    if ($version->version_extra != 'dev') {
      $is_valid = FALSE;
      form_set_error('version_extra', t('The %extra must be "dev" for snapshot releases from a branch (or HEAD).', array('%extra' => 'Extra identifier')));
    }
  }
  if ($is_valid) {
    $version->pid = $project->nid;
    $existing_nid = project_release_exists($version);
    if (!empty($existing_nid)) {
      // TODO: is there a better form element to mark with this error?
      form_set_error('version_patch', t('This version <a href="@release_url">already exists</a> for this project.', array('@release_url' => url('node/' . $existing_nid))));
      $is_valid = FALSE;
    }
  }
  return $is_valid;
}
