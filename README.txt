$Name$

By setting up scripts in the CVSROOT hooks (loginfo, commitinfo,
taginfo) this module stores CVS commit messages and makes them
available on the web site. It also provides access control to the CVS
repository based on Drupal project nodes.

Features:
  * RSS feed of the most recent changes.
  * Digest of changes to mail recipients (requires cron).
  * Includes sample PHP scripts in "xcvs" directory to provide
    integration with the database, and access control.
  * Includes sample Perl loginfo script: cvs-to-sql.pl [out of date].
  * Commit messages can be filtered and searched.

Themeability:
  * Page is wrapped in a CSS class called 'cvs'.
  * Each commit message can be modified in a theme_cvs_entry($cvs) function,
    the parameter is an object containing user, timestamp, and message.

Site-specific customization:
  * Parts of this code are very specific to how this module is used
    on drupal.org. I'm trying to isolate as many of these
    site-specific hacks as possible into the cvs_local.inc file.

Note that you must customize the xcvs/xcvs-config.php file if you wish
to use those scripts on your site. See xcvs/README.txt for details.
