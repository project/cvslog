#!/usr/bin/php
<?php

/**
*
* @file
* Connects to the Drupal database of a site using the CVS integration module
* and generates the CVSROOT/passwd file necessary for users to authenticate to
* the CVS repository using "cvs login".
*
* This script should be periodically run from cron (outside of the Drupal
* site) and the output sent to the CVSROOT/passwd file for the CVS repository
* (or repositories) where your Drupal users with approved CVS accounts should
* have access. There are some PHP constants that must be properly configured
* for your site before this script will work.
*/

/// Hostname where your database is running.
define('DB_HOST', 'localhost');

/// Database user to connect as.
define('DB_USER', '');

/// Password for the database user.
define('DB_PASSWORD', '');

/// The name of the database for your Drupal site.
define('DB_NAME', '');

/**
 * The UNIX username to include in the generated passwd file that all CVS
 * commands will run as.
 */
define('CVS_USER', 'cvsuser');

/**
 * The base of your Drupal site (used to generate comments with links to user
 * accounts).  For example "drupal.org".
 */
define('DRUPAL_SITE', '');

/**
 * general error function
 */
function error_message($msg = '') {
  global $argv;
  $stderr = fopen('php://stderr', 'w');
  fwrite($stderr, "ERROR: " . basename($argv[0]) . ": " . $msg . "\n");
  fclose($stderr);
}

/**
 * Database error function
 */
function database_error_check($msg = '') {
  $errno = mysql_errno();
  $error = mysql_error();
  if ($errno) {
    error_message($msg);
    error_message("More info: $error (errno: $errno)");
    exit(1);
  }
}

if ($argc == 2) {
  $passwd = $argv[1];
  $passwd_new = $argv[1] . '.new';
}
else {
  error_message('Usage error: required argument missing: full path name for CVSROOT/passwd file');
  exit(2);
}

// Connect to the right database.
$connection = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
mysql_select_db(DB_NAME);
database_error_check('Could not connect to database.');

// Generate a login for the anonymous user:
$output = "anonymous:QO9EJSIJc3kwg:". CVS_USER ."\n\n";

$result = mysql_query("SELECT * FROM cvs_accounts WHERE pass != '' AND status = 1 ORDER BY cvs_user");
database_error_check('SELECT query failed.');
while ($account = mysql_fetch_object($result)) {
  if ($account->uid) {
    $output .= "# http://". DRUPAL_SITE ."/user/$account->uid\n";
  }
  else {
    $output .= "# unknown user\n";
  }
  $output .= "$account->cvs_user:$account->pass:". CVS_USER ."\n\n";
}

// Write the results out to the new file
if (!file_put_contents($passwd_new, $output)) {
  error_message('Writing to new passwd file failed!');
  exit(3);
}

// Rename the file
if (!rename($passwd_new, $passwd)) {
  error_message('Renaming new passwd file failed!');
  exit(4);
}
