#!/usr/bin/php
<?php
/**
* $Name$
*
* @file
* Provides access checking and validation for cvs tag commands.
*
* @author Kjartan (originally). Massively re-written and ported to use
* xcvs-db.php by Derek Wright "dww" (http://drupal.org/user/46549)
* in 2006-05.
*
* Exit status:
*   0 - OK
*   1 - Access denied
*   2 and above - Errors (see xcvs_db_check_write_access() for details)
*
*/

function xcvs_help($cli) {
  $output = "Usage: $cli <config file> \$USER\n\n";
  print $output;
}

function xcvs_valid_dir($directory, $xcvs) {
  if (count($xcvs["tag_directory"])) {
    foreach ($xcvs["tag_directory"] as $regexp) {
      if (preg_match($regexp, $directory)) {
        return 1;
      }
    }
    return 0;
  }
  // Don't validate directories
  return 1;
}

function xcvs_valid_tag($tag, $dir, $type, $op, $xcvs) {
  if ($op == "del") {
    // We should allow users to delete any old tags so long as they
    // are allowed via xcvs_db_check_write_access(). If there are
    // invalid tags, we want to give users the power to delete them
    // regardless of the regexps that enforce conventions.
    return 1;
  }
  $is_branch = $type == 'T' ? 1 : 0;

  if ($is_branch) {
    $regexp_list = $xcvs['valid_branches'];
    $err_msg = $xcvs['invalid_branch_message'];
    $other_regexp_list = $xcvs['valid_tags'];
    $other_err_msg = $xcvs['invalid_branch_valid_tag_message'];
  }
  else {
    $regexp_list = $xcvs['valid_tags'];
    $err_msg = $xcvs['invalid_tag_message'];
    $other_regexp_list = $xcvs['valid_branches'];
    $other_err_msg = $xcvs['invalid_tag_valid_branch_message'];
  }

  if (count($regexp_list)) {
    foreach ($regexp_list as $regexp) {
      if (preg_match($regexp, $tag)) {
        return 1;
      }
    }
    // If we're about to fail, see if this would be a valid tag/branch:
    $other = false;
    foreach ($other_regexp_list as $regexp) {
      if (preg_match($regexp, $tag)) {
        $other = true;
        break;
      }
    }
    if ($other && !empty($other_err_msg)) {
      print strtr($other_err_msg, array('%dir' => $dir, '%tag' => $tag));
    }
    else {
      print strtr($err_msg, array('%dir' => $dir, '%tag' => $tag));
    }
    return 0;
  }
  // Don't validate tags
  return 1;
}

function xcvs_send_tag_email($user, $dir, $tag, $op, $xcvs) {
  if (!$headers) {
    $headers = "From: $xcvs[logs_from]\n";
  }
  $headers .= "X-Sender: $xcvs[logs_from]\n";
  $headers .= "X-Mailer: eXtendedCVS v$xcvs[version]\n";
  $headers .= "X-Priority: 3\n";
  $headers .= "Reply-To: $xcvs[logs_from]\n";
  $headers .= "Content-Type: text/plain; charset=UTF-8; format=flowed\n";
  $headers .= "Content-Transfer-Encoding: 8Bit";

  $operations = array("add" => "Added", "mov" => "Moved", "del" => "Deleted");
  $operation = $operations[$op];
  $parts = explode("/", $dir, 2);
  $module = $parts[0];
  $dir = "/$parts[1]";

  $output = "User: $user\tBranch: ". ($tag ? $tag : "HEAD") ."\tDate: ". gmdate('r');
  $output .= "\n\nLog message:\n";
  $output .= "\n  - $operation $tag.\n";

  mail(
    $xcvs["logs_mailto"],
    strtr($xcvs["logs_subject"], array("%module" => ($tag ? "$module($tag)" : $module), "%user" => $user, "%files" => "$dir")),
    $output,
    $headers
  );
}

function xcvs_store_tag_in_db($dir, $tag, $type, $op, $xcvs) {
  // Integrate with Drupal cvslog.module by storing tag info in the
  // {cvs_tags} table.

  // Store the appropriate value for if this tag is a branch or not.
  // CVS will specify 'T' if this tag is a branch, 'N' if it's not,
  // or '?' if the tag is being deleted. *sigh*  All we care is if
  // we're adding a branch, so that's all we check for here.
  $branch = $type == 'T' ? 1 : 0;

  $connection = xcvs_db_connect($xcvs);

  // Find the project nid that this directory belongs to
  $nid = xcvs_db_get_drupal_project_nid($dir, $xcvs);

  // Lock the affected tables so avoid clashes (transactions):
  mysql_query('LOCK TABLES cvs_tags');

  if ($op == 'add') {
    mysql_query("INSERT INTO cvs_tags (nid, tag, branch, timestamp) VALUES ($nid, '" . mysql_real_escape_string($tag) . "', $branch, ". time() .")");
  }
  else {
    mysql_query("DELETE FROM cvs_tags WHERE nid=$nid AND tag='" . mysql_real_escape_string($tag) . "'");
  }

  // Unlock the affected tables so avoid clashes (transactions):
  mysql_query('UNLOCK TABLES');
  mysql_close($connection);
}

function xcvs_init($argc, $argv) {
  if ($argc < 5) {
    xcvs_help($argv[0]);
    exit(2);
  }

  // Load configuration file
  if (!file_exists($argv[1])) {
    exit("Error: failed to load configuraton file.");
  }
  include_once $argv[1];
  $user = $argv[2];
  $tag = $argv[3];
  $type = $argv[4];
  $op = $argv[5];
  $dir = $argv[6];

  if ($xcvs['tag_import_in_progress']) {
    print $xcvs['tag_import_message'];
    exit(1);
  }

  if (!xcvs_always_allow($user, $xcvs)) {
    // If this isn't a whitelisted superuser, do some checks...
    if ($op == "del" && !$xcvs['allow_tag_removal']) {
      print $xcvs['tag_delete_denied_message'];
      exit(1);
    }
    if (!(xcvs_valid_dir($dir, $xcvs))) {
      print "\nTags not allowed in /$dir.\n\n";
      exit(1);
    }
    if (!(xcvs_valid_tag($tag, $dir, $type, $op, $xcvs))) {
      // Appropriate error is already printed in xcvs_valid_tag()
      exit(1);
    }

    // if the tag is valid, see if we want to do DB-based access
    // checking, and if so, perform the check.  this will exit if
    // there's any sort of error
    if ($xcvs['db_access_check']) {
      xcvs_db_check_write_access($user, $dir, 'tag', $xcvs);
    }

    // if we made it this far, make sure they're not trying to
    // delete/move a tag that has a release node pointing to it
    if (($op=='mov' || $op=='del') && $xcvs['prevent_release_tag_changes']) {
      xcvs_db_check_release_tag($dir, $tag, $op, $xcvs);
    }
  }

  // If we got this far, we're going to allow the tag...
  if (!empty($xcvs["logs_mailto"])) {
    xcvs_send_tag_email($user, $dir, $tag, $op, $xcvs);
  }
  if (!empty($xcvs["store_tags_in_db"]) && $op != 'mov') {
    xcvs_store_tag_in_db($dir, $tag, $type, $op, $xcvs);
  }

  if (!empty($xcvs["branch_message"]) && $type == 'T') {
    // CVS will specify 'T' for the type argument if it's a branch. *sigh*
    print strtr($xcvs["branch_message"], array('%branch' => $tag));
  }

  exit(0);
}

xcvs_init($argc, $argv);

?>
