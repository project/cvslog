#!/usr/bin/php
<?php
/**
* $Name$
*
* @file
* Provides access checking and other hooks for cvs commits.
*
* @author Kjartan (originally). Massively re-written and ported to use
* xcvs-db.php by Derek Wright "dww" (http://drupal.org/user/46549)
* in 2006-05.
*
* Exit status:
*   0 - OK
*   1 - Access denied
*   2 and above - Errors (see xcvs_db_check_write_access() for details)
*
*/

function xcvs_help($cli) {
  $output = "Usage: $cli <config file> \$USER\n\n";
  print $output;
}

function xcvs_check_dir(&$dir) {
  // Strip trailing slashes:
  $dir = preg_replace('/\\/+$/', '', $dir);
  return is_dir($dir);
}

function xcvs_log_add($filename, $dir, $mode = "w") {
  $fd = fopen($filename, $mode);
  fwrite($fd, $dir);
  fclose($fd);
}

function xcvs_allow_commit($dir, &$files, $xcvs) {
  foreach ($files as $file) {
    if (xcvs_deny_file_commit("$dir/$file", $xcvs)) {
      print strtr($xcvs['file_denied_error'], array('%file' => "$dir/$file"));
      exit (4);
    }
    if (!xcvs_allow_file_commit("$dir/$file", $xcvs)) {
      return 0;
    }
  }
  return 1;
}

function xcvs_allow_file_commit($file, $xcvs) {
  if (count($xcvs['commit_allow_files'])) {
    foreach ($xcvs['commit_allow_files'] as $regexp) {
      if (preg_match($regexp, $file)) {
        return 1;
      }
    }
  }
}

function xcvs_deny_file_commit($file, $xcvs) {
  if (count($xcvs['commit_deny_files'])) {
    foreach ($xcvs['commit_deny_files'] as $regexp) {
      if (preg_match($regexp, $file)) {
        return 1;
      }
    }
  }
}

/**
 * See if the current commit has a sticky tag, and if so, validate that it's a
 * valid branch.
 */
function xcvs_validate_branch_on_commit($xcvs) {
  if (!is_dir('CVS')) {
    echo "ERROR: No local CVS directory during commit, aborting\n\n";
    exit(6);
  }
  if (file_exists('CVS/Tag')) {
    // There's a sticky tag, validate it.
    $tag = '';
    $tag_file = trim(file_get_contents('CVS/Tag'));
    if ($tag_file) {
      // Get the sticky tag for this commit: strip off the leading 'T or N'.
      $tag = preg_replace('@^(T|N)@', '', $tag_file);
    }
    if (!empty($tag) && !_xcvs_is_valid_branch($xcvs, $tag)) {
      // Not valid, abort with a meaningful error message.
      print strtr($xcvs['invalid_branch_on_commit_message'], array('%branch' => $tag, '%errors' => ''));
      exit (5);
    }
  }
  // To be extra paranoid, check everything in CVS/Entries, too.
  if (file_exists('CVS/Entries')) {
    $entries = file('CVS/Entries');
    if (!empty($entries)) {
      $errors = array();
      foreach ($entries as $entry) {
        $parts = explode('/', trim($entry));
        if (!empty($parts[5])) {
          $tag = preg_replace('@^(T|N)@', '', $parts[5]);
          if (!_xcvs_is_valid_branch($xcvs, $tag)) {
            $errors[] = $parts[1] .': '. $tag;
          }
        }
      }
      if (!empty($errors)) {
        print strtr($xcvs['invalid_branch_on_commit_message'], array('%branch' => $tag, '%errors' => implode("\n** ", $errors)));
        exit (5);
      }
    }
  }
}

function _xcvs_is_valid_branch($xcvs, $branch) {
  foreach ($xcvs['valid_branches'] as $regexp) {
    if (preg_match($regexp, $branch)) {
      // It's valid, we're done.
      return TRUE;
    }
  }
  return FALSE;
}

function xcvs_init($argc, $argv) {
  if ($argc < 5) {
    xcvs_help($argv[0]);
    exit(2);
  }

  // Load configuration file
  if (!file_exists($argv[1])) {
    print("Error: failed to load configuration file.\n");
    exit(1);
  }
  include_once $argv[1];
  $user = $argv[2];
  $dir = substr($argv[3], strlen($_ENV["CVSROOT"]) + 1);
  $files = array_slice($argv, 4);

  // Check temporary file storage
  if (!xcvs_check_dir($xcvs["temp"]) && !is_writeable($xcvs["temp"])) {
    exit("Error: failed to access the temporary directory.");
  }

  if ($xcvs['validate_branch_on_commit'] && !xcvs_always_allow($user, $xcvs)) {
    // This will abort with an error message if the branch is invalid.
    xcvs_validate_branch_on_commit($xcvs);
  }

  if (!xcvs_always_allow($user, $xcvs)
      && !xcvs_allow_commit($dir, $files, $xcvs)
      && $xcvs["db_access_check"]) {
    // if this is neither a super user or a file we always allow, and
    // if we're configured for db access checks, do the check. This
    // will exit if there's an error of any kind.
    xcvs_db_check_write_access($user, $dir, 'commit', $xcvs);
  }

  if ($xcvs["logs_combine"]) { // Do we combine log messages?
    $lastlog = $xcvs["temp"] ."/xcvs-lastlog.". posix_getpgrp();
    $difflog = $xcvs["temp"] ."/xcvs-difflog.". posix_getpgrp();
    if ($xcvs["logs_diff"] && !file_exists($difflog)) {
      `cvs -nQ diff -Nu -F^f > $difflog`;
    }
    xcvs_log_add($lastlog, $dir);
  }

  exit(0);
}

xcvs_init($argc, $argv);

?>
