<?php
/**
* $Name$
*
* @file
* Implements a number of helper methods used for quering the drupal
* database to find out if a given CVS operation should be allowed.
*
* @author Derek Wright "dww" (http://drupal.org/user/46549)
*
*/

/**
 * Opens a MySQL connection with the right DB selected. All other
 * methods in this file (except for xcvs_always_allow(), by design)
 * require that this method has been called first.
 */
function xcvs_db_connect($xcvs) {
  static $db_conn;
  if (!isset($db_conn)) {
    $url = parse_url($xcvs['db_url']);
    // Allow for non-standard MySQL port.
    if (isset($url['port'])) {
      $url['host'] = $url['host'] .':'. $url['port'];
    }
    $db_conn = mysql_connect($url['host'], $url['user'], $url['pass']);
    mysql_select_db(substr($url['path'], 1));
  }
  return $db_conn;
}

/**
 * Returns the numeric drupal uid for the given cvs username
 */
function xcvs_db_get_drupal_uid($user) {
  // See if there's an approved CVS account that matches this CVS username.
  $result = mysql_query("SELECT uid FROM cvs_accounts WHERE cvs_user = '". mysql_real_escape_string($user) ."' AND status = 1");
  if ($result && mysql_num_rows($result) > 0) {
    $uid = mysql_result($result, 0);
  }
  else {
    $uid = 0;
  }
  return $uid;
}

/**
 * Return a new unique ID in the given sequence.
 */
function xcvs_last_insert_id($table, $field) {
  return mysql_result(mysql_query("SELECT LAST_INSERT_ID()"), 0);
}

/**
 * Determines the numeric drupal node id (nid) for the project a given
 * file belongs to.
 */
function xcvs_db_get_drupal_project_nid($file, $xcvs) {
  static $nids = array();
  if (!isset($nids[$file])) {
    $parts = explode("/", $file);

    // First, get rid of the "contributions" part
    array_shift($parts);

    // Now, we loop over the path, using the most restrictive path
    // first, and query the DB to find a matching published project node.
    while (count($parts) > 1) {
      $project_dir = '/'. implode('/', $parts) . '/';
      array_pop($parts);
      $project = mysql_query("SELECT n.nid FROM cvs_projects cp INNER JOIN node n ON n.nid = cp.nid WHERE n.status = 1 AND cp.rid = ". $xcvs[cvs_repo_id] ." AND cp.directory = '". mysql_real_escape_string($project_dir) ."'");
      if ($project && mysql_num_rows($project) > 0) {
        return $nids[$file] = mysql_result($project, 0);
      }
    }
    // If we didn't find it already, cache the answer as not-found
    $nids[$file] = 0;
  }
  return $nids[$file];
}

/**
 * Determine if the given cvs user should have CVS write access
 * (commits or tags) to the requested file. Checks if the user is the
 * owner of the project that the requested file belongs to.  If not,
 * sees if the user is listed as a CVS maintainer for the project in
 * question.
 *
 * The real work of this method is performed by another function.
 * This is a wrapper to handle calling exit() if there was an error
 * (see _xcvs_db_check_write_access() for possible values).
 *
 * @return
 *   0 on success, exit() with non-zero on error.
 */
function xcvs_db_check_write_access($user, $file, $op, $xcvs) {
  xcvs_db_connect($xcvs);
  $result = _xcvs_db_check_write_access($user, $file, $op, $xcvs);
  if ($result) {
    exit($result);
  }
  return 0;
}

/**
 * Helper method that performs the actual checks for the given user
 * and file. If there's an error, this method is responsible for
 * printing out the details of what went wrong. However, the caller is
 * responsible for calling exit(), so that it can close the DB
 * connection first.
 *
 * @return
 *   Integer result code (used as the exit value of the script).
 *   0: success (operation granted)
 *   1: permission denied
 *   2: cvs user can't be mapped to a drupal user
 *   3: file can't be mapped to a drupal project
 */
function _xcvs_db_check_write_access($user, $file, $op, $xcvs) {
  $uid = xcvs_db_get_drupal_uid($user);
  if (!$uid) {
    print strtr($xcvs['user_not_found_error'], array('%user' => $user));
    return 2;
  }
  if (xcvs_db_is_cvs_superuser($uid)) {
    // NOTE: this particular check is not yet implemented. For now, we
    // just rely on the $xcvs['cvs_allow_users'] whitelist, instead.
    return 0;
  }
  $nid = xcvs_db_get_drupal_project_nid($file, $xcvs);
  if (!$nid) {
    // if there's no published project node for this directory, allow
    // commits but not tags
    if ($op == 'commit') {
      return 0;
    }
    else {
      print strtr($xcvs['project_not_found_error'], array('%dir' => $file));
      return 3;
    }
  }
  if (xcvs_db_is_owner($uid, $nid)) {
    return 0;
  }
  if (xcvs_db_is_maintainer($uid, $nid)) {
    return 0;
  }
  print strtr($xcvs['access_error'], array('%user' => $user, '%op' => $op, '%dir' => $file));
  return 1;
}

/**
 * Determines if the user is the owner of the given project.
 */
function xcvs_db_is_owner($uid, $nid) {
  $project = mysql_query("SELECT nid, uid FROM node WHERE nid = " . (int)$nid ." AND uid = " . (int)$uid);
  if ($project && mysql_num_rows($project) > 0) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Determines if the user is a CVS maintainer for the given project.
 */
function xcvs_db_is_maintainer($uid, $nid) {
  $project = mysql_query("SELECT * FROM cvs_project_maintainers WHERE nid = " . (int)$nid ." AND uid = " . (int)$uid);
  if ($project && mysql_num_rows($project) > 0) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Determines if the given user is a CVS superuser.
 * Would have to check for roles and permissions, etc.
 */
function xcvs_db_is_cvs_superuser($uid) {
  // TODO!
  return FALSE;
}

function xcvs_db_check_release_tag($dir, $tag, $op, $xcvs) {
  // Make sure we're connected, just to be safe.
  xcvs_db_connect($xcvs);
  $project_nid = xcvs_db_get_drupal_project_nid($dir, $xcvs);
  $query = mysql_query("SELECT nid FROM project_release_nodes WHERE pid = " . (int)$project_nid ." AND tag = '" . mysql_real_escape_string($tag) . "'");
  if ($query && mysql_num_rows($query) > 0) {
    $release = mysql_fetch_object($query);
    print strtr($xcvs['release_tag_in_use_message'], array('%tag' => $tag, '%nid' => $release->nid));
    exit(4);
  }
}

/**
 * Determines if the given user is on the whitelist of users that are
 * automatically granted all cvs powers, even if the Drupal DB is
 * unavailable, etc.
 */
function xcvs_always_allow($user, $xcvs) {
  if (is_array($xcvs['cvs_allow_users'])) {
    return in_array($user, $xcvs['cvs_allow_users']);
  }
  return FALSE;
}

