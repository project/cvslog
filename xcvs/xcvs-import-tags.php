<?php
/**
 * $Name$
 *
 * @file
 * Parses through an entire CVS repository tree, finds all the
 * tags/branches, and inserts them into the {cvs_tags} table.
 */

include_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

// If not in 'safe mode', increase the maximum execution time:
if (!ini_get('safe_mode')) {
  set_time_limit(3600);
}

if (!module_exist('cvs')) {
  print '<b>' . t('ERROR: xcvs-import-tags.php requires that you
  first install the cvs.module') . '</b>';
  exit(1);
}

// Pull in the code for cvs.module, for all the real code.
$path = drupal_get_path('module', 'cvs');
if (file_exists("$path/cvs.module")) {
  require_once "$path/cvs.module";
}

// Actual work
print "Starting to import tag info<br>";
$start_time = time();

// Specify which repositories to import (based on the "rid" field in
// the {cvs_repositories} table.
$repos_to_import = "1,2";

$result = db_query("SELECT * FROM {cvs_repositories} WHERE rid IN (%s)", $repos_to_import);
while ($repo = db_fetch_object($result)) {
  cvs_fetch_repository($repo, TRUE);
}

$num_imported = db_result(db_query("SELECT COUNT(*) FROM {cvs_tags}"));
print t('Imported %num tags in %interval', array('%num' => $num_imported, '%interval' => format_interval(time() - $start_time)));
