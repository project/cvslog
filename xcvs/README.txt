$Name$

This directory contains a set of scripts that can be used via the
hooks provided by CVS to enable access control for commit and tag
operations, and to record log messages and other information into the
Drupal database for proper integration with the cvs.module.

These scripts (or something like them) are required if you
enable the "Use external script to insert data" option on the
"admin/cvs/repositories/add" page after you have installed the
cvs.module into your Drupal site.

Originally written by Kjartan Mannes (http://drupal.org/user/2).
Major re-write to include access control and other changes
by Derek Wright (http://drupal.org/user/46549).

Any bug reports or feature requests should be submitted to the
cvs.module issue queue: http://drupal.org/project/issues/cvslog

Each script, and the CVSROOT hook required for its use, are described
below:

--------------------
xcvs-config.php
--------------------

  A configuration file that all of the other scripts depend on. The
  file is heavily commented with instructions. There are a few
  required settings at the top which must be customized to your site
  before anything else will work.


--------------------
xcvs-commitinfo.php
--------------------

  A script to enforce access control for CVS commit operations and to
  assemble log messages that span multiple directories into a single
  notification email.  To enable this, you must add this line to your
  CVSROOT/commitinfo file:

ALL $CVSROOT/CVSROOT/xcvs-commitinfo.php $CVSROOT/CVSROOT/xcvs-config.php $USER %r/%p %s


--------------------
xcvs-taginfo.php
--------------------

  A script to enforce access control for CVS tag operations and to
  optionally send notification emails for each cvs tag.  To enable
  this, you must add this line to your CVSROOT/taginfo file:

ALL $CVSROOT/CVSROOT/xcvs-taginfo.php $CVSROOT/CVSROOT/xcvs-config.php $USER %t %b %o %p %{sv}

  NOTE: In revision 1.1 of the xcvs-taginfo.php file, the %b was *NOT*
  used. However, if you upgrade to any later version, the %b *MUST* be
  specified.  Furthermore, you must upgrade your copy of cvs.install
  to at least revision 1.4.2.2 and run cvs_update_3() from update.php.
  Additionally, to import all previous branch and tag information, you
  should install and run xcvs-import-tags.php (described below).


--------------------
xcvs-loginfo.php
--------------------

  A script to insert all cvs log messages into your Drupal database to
  integrate with the functionality provided by the cvs.module.  To
  enable this, you must add this line to your CVSROOT/loginfo file:

ALL $CVSROOT/CVSROOT/xcvs-loginfo.php $CVSROOT/CVSROOT/xcvs-config.php $USER %1{sVv}


--------------------
xcvs-db.php
--------------------

  This script contains database access helper methods that are shared
  by all other scripts. This is included automatically, and requires
  no customization or changes to the CVSROOT hook files.


--------------------
xcvs-import-tags.php
--------------------

  This script is a 1-time update script to import all historic tag and
  branch info from all configured CVS repositories into the {cvs_tags}
  table. It should only be run if you have been using the cvs.module
  from before 2006-09-18 (cvs.module revision 1.106.2.18 and older on
  the DRUPAL-4-7 branch, or revision 1.123 and older on the TRUNK). To
  run it, put a copy of the script in the document root of your web
  site (where the main drupal index.php script lives), and navigate
  your browser to that script. If you have a very active CVS
  repository, you might want to disable tags during the update, which
  can take a few minutes to run (depending on your hardware and the
  size of the CVS repositories that need to be imported). To do so,
  set the $xcvs['tag_import_in_progress'] variable in xcvs-config.php
  to TRUE, which disable all tag operations with a warning message.

