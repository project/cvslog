<?php
/**
* $Name$
*
* @file
* Configuration variables and settings for eXtended CVS.
*/

// ------------------------------------------------------------
// Required customization
// ------------------------------------------------------------

// Drupal database URL
$xcvs['db_url'] = 'mysql://USERNAME:PASSWORD@HOSTNAME/DBNAME';

// File location where to store temporary files.
$xcvs["temp"] = "/tmp";

// Directory where cvs commit log emails should be sent. If not
// specified, no email is sent.
$xcvs["logs_mailto"] = '';

// From header to use for commit log emails.
$xcvs["logs_from"] = '';

// Drupal repository id that this installation of scripts is going to
// interact with. If you only have one repository, leave this as '1'.
// If you have multiple repositories installed via the cvs.module, you
// can find the appropriate value at the "admin/cvs/repositories" page
// on your site. Click on the "edit" link, and notice the final number
// in the resulting URL.
$xcvs["cvs_repo_id"] = 1;


// ------------------------------------------------------------
// Optional customization
// ------------------------------------------------------------

// URL to include in email messages to provide a link to view the diff.
// This url can contain:
// %file => full filename of this commit
// %old  => the previous revision number of this file
// %new  => the new revision number of this file
// If not set, no such link is included in commit email messages.
// EXAMPLE: On cvs.drupal.org, we run ViewCVS:
//$xcvs["logs_cvsurls"] = "http://cvs.drupal.org/diff.php?path=%file&old=%old&new=%new&root=contrib";
$xcvs["logs_cvsurls"] = '';

// Same as above, but used only for new files. For example, when using
// ViewsCVS, you need different arguments for new files to avoid an
// "Invalid revision(s) passed to diff" error.
// EXAMPLE: On cvs.drupal.org, we run ViewCVS:
//$xcvs["logs_cvsurls_initial"] = "http://cvs.drupal.org/diff.php?path=%file";
$xcvs["logs_cvsurls_initial"] = '';

// Should these scripts update the Drupal database with commit logs
// and information to provide cvslog.module integration?
$xcvs["cvslog"] = TRUE;

// Combine the commit log messages for a multidir commit into one mail.
$xcvs["logs_combine"] = TRUE;

// Subject used for log emails (for both tags and commits).
$xcvs["logs_subject"] = "[%module:%user] %files";

// Should these scripts store tag info in the {cvs_tags} table?
// NOTE: To enable this functionality, you must upgrade the cvs.install
// file to at least revision 1.4.2.2 and run cvs_update_3().
$xcvs["store_tags_in_db"] = TRUE;


// ------------------------------------------------------------
// Commit access control
// ------------------------------------------------------------

// Enable access control that queries the Drupal database. If a CVS
// user is attempting an operation (tag, commit) on a file that has a
// Drupal project node associated with it, only the owner of that
// node, or users that have been explicitly granted access with the
// "cvs-access" tab will be allowed.
$xcvs['db_access_check'] = TRUE;

// These users are always allowed full access, even if we can't
// connect to the DB. This optional list should contain the CVS
// usernames (not the Drupal username if they're different).
$xcvs['cvs_allow_users'] = array();

// List of regular expressions describing files where commits are
// always denied, regardless of other access control. If empty,
// files will be tested with the other access control checks.
// EXAMPLE: On drupal.org, we prevent any files from being committed
// to the profiles directory, except things that end in .txt or .profile.
// We also prevent any packaged files (.gz, .tar, etc).
//$xcvs['commit_deny_files'] = array('@^contributions/profiles.*(?<!\.profile|\.txt)$@', '@^.*\.(gz|tgz|tar|zip)$@');
$xcvs['commit_deny_files'] = array();

// List of regular expressions describing files where commits are
// always allowed, regardless of other access control. If empty,
// commits to directories that have a Drupal project associated with
// them are only allowed to users who have been explicitly granted
// repository access for that project.
// EXAMPLE: On drupal.org, we allow anyone with a CVS account to
// commit .po or .pot files, which are used to store translations.
// Similarly, anyone is allowed to commit changes to the documentation,
// and sandbox directories.
//$xcvs['commit_allow_files'] = array("@.*\.(po|pot)@", "@^contributions/(docs|sandbox|tricks)@");
$xcvs['commit_allow_files'] = array();


// ------------------------------------------------------------
// Tag access control
// ------------------------------------------------------------

// List of regular expressions for tag names that should be allowed.
// If this is empty, all tags are allowed.
// EXAMPLE: On cvs.drupal.org, we require that tags start with a Drupal core
// compatibility version (e.g. "DRUPAL-5"), two hypens ("--"), a major version
// number, another hypen, a patch-level number, and optionally, another hyphen
// and some "extra" (e.g. "-BETA1") that can only contain uppercase letters
// and numbers:
// $xcvs['valid_tags'] = array('@^DRUPAL-[56]--(\d+)-(\d+)(-[A-Z0-9]+)?@');
$xcvs['valid_tags'] = array();

// List of regular expressions for branches that should be allowed
// If this is empty, all branches are allowed.
// EXAMPLE: On cvs.drupal.org, we require that branches start with a Drupal
// core compatibility version (e.g. "DRUPAL-6"), two hypens ("--"), a major
// version number.  For DRUPAL-5 and earlier, the "--1" isn't allowed, only
// "--2" and higher, and the "DRUPAL-5" branch was valid.  So, there are two
// regular expressions used, one for 5.x and earlier, one for 6.x and beyond:
// $xcvs['valid_branches'] = array('@^DRUPAL-5(--[2-9])?$@', '@^DRUPAL-6--[1-9]$@')
$xcvs['valid_branches'] = array();

// List of regular expressions for directories where tags should be
// allowed. If empty, all directories can be tagged.
// EXAMPLE: On drupal.org, only allow tags in the modules, themes,
// theme-engines, docs and translations directories.
// $xcvs['tag_directory'] = array("@[^/]+?/(modules|themes|theme-engines|docs|translations)(/.+)?@");
$xcvs['tag_directory'] = array();

// Boolean to specify if users should be allowed to delete tags.
$xcvs['allow_tag_removal'] = TRUE;

// Boolean to specify if users should be allowed to delete or move
// tags that have release nodes pointing to them. This assumes that
// you are using at least version 4.7.x-2.* of project.module and
// have release nodes on your site.
$xcvs['prevent_release_tag_changes'] = TRUE;

// Boolean to specify if tags should be disabled while historical
// information is imported into the database. Only set this to TRUE
// if you are going to run xcvs-import-tags.php. You can customize the
// error message via $xcvs['tag_import_message'] (see below).
$xcvs['tag_import_in_progress'] = FALSE;

// Boolean to specify if branches should be validated during commits.
// This prevents bogus branches from being introduced when adding 
// directories or files with sticky tags, which implicitly creates a 
// branch without invoking cvs tag (therefore skipping xcvs-taginfo).
$xcvs['validate_branch_on_commit'] = TRUE;


// ------------------------------------------------------------
// Error messages
// ------------------------------------------------------------

$xcvs["access_error"] = <<<EOF
** Access denied: %user does not have permission to %op files in:
** %dir
** Please contact the owner of this project and request to be added
** as a CVS maintainer, see http://drupal.org/node/63634.

EOF;

$xcvs["file_denied_error"] = <<<EOF
** Access denied: this file is not allowed:
** %file

EOF;

$xcvs["project_not_found_error"] = <<<EOF
** ERROR: no published project was found for this directory:
** %dir
** Please contact a CVS administrator for help.

EOF;

$xcvs["user_not_found_error"] = <<<EOF
** ERROR: no Drupal user matches cvs user '%user'.
** Please contact a CVS administrator for help.

EOF;

$xcvs['invalid_tag_message'] = <<<EOF
** ERROR: invalid tag for this directory:
** %dir

EOF;

$xcvs['invalid_branch_message'] = <<<EOF
** ERROR: invalid branch for this directory:
** %dir

EOF;

$xcvs['invalid_tag_valid_branch_message'] = <<<EOF
** ERROR: "%tag" is a valid name for a branch, but not a tag. You
** must either use the -b option for your cvs tag command if you
** wish to create a branch, or choose a valid tag name.

EOF;

$xcvs['invalid_branch_valid_tag_message'] = <<<EOF
** ERROR: "%tag" is a valid name for a tag, but not a branch. You
** must either remove the -b option from your cvs tag command if you
** do not want to create a branch, or choose a valid branch name.

EOF;

$xcvs['invalid_branch_on_commit_message'] = <<<EOF
** ERROR: Your commit is using a 'Sticky tag' which corresponds to
** a branch which is not valid in this CVS repository. If this
** commit were to proceed, you would create an invalid branch:
** %branch
** %errors

EOF;

$xcvs['tag_delete_denied_message'] = <<<EOF
** ERROR: You are not allowed to delete tags.

EOF;

$xcvs['release_tag_in_use_message'] = <<<EOF
** ERROR: You are not allowed to delete or move tags that have
** release nodes pointing to them. "%tag" is being used by
** http://[your_site_here]/node/%nid

EOF;

$xcvs['tag_import_message'] = <<<EOF
** ERROR: CVS tag operations are currently disabled while
** historical branch and tag informating is being imported.
** Please be patient and try again in a few minutes.

EOF;

$xcvs['branch_message'] = <<<EOF
** NOTE: Don't forget that creating a branch does NOT
** automatically update your workspace to use that branch.
** If you want to commit to this new branch, you must run:
** cvs update -r %branch

EOF;


// ------------------------------------------------------------
// Internal code
// ------------------------------------------------------------

// This code includes the DB-specific functionality for the other
// tools to use, do not remove these lines.
$xcvs_db = "$_ENV[CVSROOT]/CVSROOT/xcvs-db.php";
if (!file_exists($xcvs_db)) {
  exit("Error: failed to load database communication file.");
}
include_once $xcvs_db;

// Version of the xcvs-* scripts. Used in email headers, for example.
// You should not edit this value.
$xcvs['version'] = "2.1";

?>
