#!/usr/bin/php
<?php
/**
* $Name$
*/

function xcvs_help($cli) {
  $output = "Usage: $cli <config file> \$USER %{sVv}\n\n";
  print $output;
}

function xcvs_check_args($argv) {
  if (strstr($argv[1], 'New directory') || strstr($argv[1], 'Imported')) {
    return 0;
  }
}

function xcvs_check_file($file) {
  if ($file) {
    list($file, $old, $new) = explode(",", $file);
    if ($old == "dir") {
      $status = "directory";
    }
    elseif ($old == "NONE") {
      $status = "added";
    }
    elseif ($new == "NONE") {
      $status = "removed";
    }
    else {
      $status = "modified";
    }
    global $xcvs_log_files;
    $xcvs_log_files[$status][$file] = array("old" => $old, "new" => $new);
  }
}

function xcvs_check_dir(&$dir) {
  // Strip trailing slashes:
  $dir = preg_replace('/\\/+$/', '', $dir);
  return is_dir($dir);
}

function xcvs_log_add($filename, $dir, $mode = "w") {
  $fd = fopen($filename, $mode);
  fwrite($fd, $dir);
  fclose($fd);
}

function xcvs_log_last($filename, $dir) {
  if (file_exists($filename)) {
    $fd = fopen($filename, "r");
    $last = fgets($fd);
    fclose($fd);
    return $dir == $last ? 1 : 0;
  }
  return 1;
}

function xcvs_log_create(&$tag) {
  $output = "";
  do {
    $line = trim(fgets(STDIN));
    if (preg_match("/^Tag:\s+(.+)$/", $line, $matches)) {
      $tag = trim($matches[1]);
    }
  } while ($line != "Log Message:");
  while (!feof(STDIN)) {
    $output .= fgets(STDIN);
  }
  $output = trim($output);

  return $output;
}

function xcvs_log_format($text) {
  return preg_replace('/^/m', '  ', $text);
}

function xcvs_init($argc, $argv) {
  if ($argc < 4) {
    xcvs_help($argv[0]);
    exit(2);
  }

  // Load configuration file
  if (!file_exists($argv[1])) {
    exit("Error: failed to load configuration file.");
  }
  include_once $argv[1];
  list($repo, $files) = explode(" ", $argv[3], 2);
  $user = $argv[2];

  // Check temporary file storage
  if (!xcvs_check_dir($xcvs["temp"]) && !is_writeable($xcvs["temp"])) {
    exit("Error: failed to access the temporary directory.");
  }

  if ($xcvs["logs_combine"]) {
    $lastlog = $xcvs["temp"] ."/xcvs-lastlog.". posix_getpgrp();
    $summary = $xcvs["temp"] ."/xcvs-summary.". posix_getpgrp();

    if ($files) {
      if ($files == '- New directory') {
        xcvs_log_add($summary, "/$repo,dir\n", "a");
      }
      else {
        foreach (explode(" ", $files) as $file) {
          xcvs_log_add($summary, "/$repo/$file\n", "a");
        }
      }
    }

    if (xcvs_log_last($lastlog, $repo)) {
      global $xcvs_log_files;
      $fd = fopen($summary, "r");
      while (!feof($fd)) {
        xcvs_check_file(trim(fgets($fd)));
      }
      fclose($fd);

      // Create e-mail body.
      foreach (array("added", "removed", "modified", "directory") as $status) {
        if (is_array($xcvs_log_files[$status])) {
          $module = "";
          $dir = "";
          if ($status == "directory") {
            $file_changes .= "\n\nNew directory:";
          }
          else {
            $file_changes .= "\n\n". ucfirst($status) ." files:";
          }
          ksort($xcvs_log_files[$status]);

          foreach ($xcvs_log_files[$status] as $file => $data) {
            preg_match("/^\/([^\/]+)(\/.*)?\/([^\/]+)$/", $file, $matches);
            $file = $matches[3];
            if ($module != $matches[1] || $dir != $matches[2]) {
              $module = $matches[1];
              $dir = $matches[2];
              $file_changes .= "\n  ". ($dir ? $dir : "/");
              $file_list .= $dir ? "$dir " : "/ ";
            }

            $file_changes .= " $file";
            $file_list .= "$file ";

            // Constuct data necessary to integrate with the Drupal cvslog.module.
            if ($xcvs['cvslog'] && $data['new']) {
              $path = "$module$dir/$file";
              exec("/usr/bin/cvs -Qn -d $_ENV[CVSROOT] rlog -N -r$data[new] $path", $cvslog, $ret);
              $matches = array();
              foreach ($cvslog as $line) {
                #date: 2004/08/20 07:51:22;  author: dries;  state: Exp;  lines: +2 -2
                if (preg_match('/^date: .+;\s+lines: \+(\d+) -(\d+)$/', $line, $matches)) {
                  break;
                }
              }

              $cvslog_file = new stdClass();
              $cvslog_file->file = "$dir/$file";
              $cvslog_file->revision = $data['new'];
              $cvslog_file->lines_added = (int)$matches[1];
              $cvslog_file->lines_removed = (int)$matches[2];

              $cvslog_files[] = $cvslog_file;
            }

            if ($xcvs["logs_cvsurls"] && !in_array($status, array("removed", "directory"))) {
              // a new file has a different url according to ViewCVS
              $diff_url = 'logs_cvsurls' . ($data["old"] == 'NONE' ? '_initial' : '');
              $links .= "  ". strtr($xcvs[$diff_url], array("%file" => "$module$dir/$file", "%old" => $data["old"], "%new" => $data["new"], "%module" => $module)). "\n";
            }
          }
        }
      }
      $message = xcvs_log_format(xcvs_log_create($tag));

      if (!empty($xcvs["logs_mailto"])) {
        $output = "User: $user\tBranch: ". ($tag ? $tag : "HEAD") ."\tDate: ". gmdate('r');
        $output .= "$file_changes\n";
        $output .= "\nLog message:\n". $message ."\n";
        if ($xcvs["logs_cvsurls"] && $links) {
          $output .= "\nLinks:\n";
          $output .= "$links";
        }
        $headers = "From: $xcvs[logs_from]\n";
        $headers .= "X-Sender: $xcvs[logs_from]\n";
        $headers .= "X-Mailer: eXtendedCVS " . $xcvs['version']. "\n";
        $headers .= "X-Priority: 3\n";
        $headers .= "Reply-To: $xcvs[logs_from]\n";
        $headers .= "Content-Type: text/plain; charset=UTF-8; format=flowed\n";
        $headers .= "Content-Transfer-Encoding: 8Bit";

        mail(
          $xcvs["logs_mailto"],
          strtr($xcvs["logs_subject"], array("%module" => ($tag ? "$module($tag)" : $module), "%user" => $user, "%files" => $file_list)),
          $output,
          $headers
        );
      }

      // Integrate with Drupal cvslog.module.
      if ($xcvs['cvslog'] && is_array($cvslog_files)) {
        $connection = xcvs_db_connect($xcvs);
        $uid = mysql_result(mysql_query("SELECT uid FROM cvs_accounts WHERE cvs_user = '". mysql_real_escape_string($user) ."'"), 0);

        mysql_query("INSERT INTO cvs_messages (rid, uid, created, cvs_user, message) VALUES ($xcvs[cvs_repo_id], $uid, ". time() .", '$user', '". mysql_real_escape_string($message) ."')");

        $cid = xcvs_last_insert_id('cvs_messages', 'cid');

        foreach ($cvslog_files as $cvslog_file) {
          $project = mysql_query("SELECT nid FROM cvs_projects WHERE rid = ". $xcvs[cvs_repo_id] ." AND '". mysql_real_escape_string($cvslog_file->file) ."' LIKE CONCAT(directory, '%')");
          if ($project && mysql_num_rows($project) > 0) $nid = mysql_result($project, 0);
          else $nid = 0;

          mysql_query("INSERT INTO cvs_files (cid, rid, uid, nid, file, branch, revision, lines_added, lines_removed) VALUES ($cid, $xcvs[cvs_repo_id], $uid, $nid, '$cvslog_file->file', '$tag', '$cvslog_file->revision', $cvslog_file->lines_added, $cvslog_file->lines_removed)");

          if (!empty($tag)) {
            // Since people refuse to learn how to use "cvs tag -b" to
            // add branches, we have an additional check here to try
            // to keep the {cvs_tags} table accurate. Every time
            // someone commits a file to a branch, we make sure
            // there's a valid entry in {cvs_tags} for that project +
            // branch, and if not, we add it here.
            $db_tag = mysql_query("SELECT * from cvs_tags WHERE nid = $nid AND tag = '" . mysql_real_escape_string($tag) . "'");
            if (!($db_tag && mysql_num_rows($db_tag) > 0)) {
              mysql_query("INSERT INTO cvs_tags (nid, branch, tag) VALUES ($nid, 1, '" . mysql_real_escape_string($tag) . "')");
            }
          }
        }

        // Invalidate the cache for the CVS maintainers block for this project.
        $cid = 'cvs_project_maintainers:'. $nid;
        mysql_query("DELETE FROM cvs_cache_block WHERE cid = '$cid'");

        mysql_close($connection);
     }

      // Clean up:
      @unlink($lastlog);
      @unlink($summary);
    }
  }
}

xcvs_init($argc, $argv);
?>
